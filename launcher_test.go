package groundcontrol

import (
	"errors"
	"log"
	"testing"
)

func testAbort(l *Launcher) {
	err := l.GetError()
	if err != nil {
		log.Println(err)
	}
	return
}

func goodfunc() error {
	return nil
}

func badfunc() error {
	return errors.New("oops")
}

func TestLauncher(t *testing.T) {
	l := NewLauncher(testAbort, SimpleInterrupt)

	l.Launch("goodfunc", goodfunc)
	l.Orbit()
	if l.err != nil {
		t.Fatal("l.err should be nil")
	}

	l.Launch("badfunc", badfunc)
	l.Orbit()
	if l.GetError() == nil {
		t.Fatal("l.err should be set")
	}
	if l.GetError().Error() != "oops" {
		t.Fatal("l.err should be correct error message")
	}
}

func BenchmarkLauncher(b *testing.B) {
	l := NewLauncher(SimpleAbort, SimpleInterrupt)
	for i := 0; i < b.N; i++ {
		l.Launch("", goodfunc)
	}
	l.Orbit()
}
