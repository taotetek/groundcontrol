package groundcontrol

import (
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

// SimpleAbort is a SIGABRT handler that just prints an error and exits
func SimpleAbort(l *Launcher) {
	if l.GetError() != nil {
		log.Println(l.GetError())
	}
	os.Exit(1)
}

// SimpleInterrupt is a SIGINT handler that just cleanly exits
func SimpleInterrupt(l *Launcher) {
	log.Println("exiting")
	os.Exit(0)
}

// Launcher controls the starting and managing of goroutines
type Launcher struct {
	orbiters         *sync.WaitGroup
	lock             *sync.RWMutex
	abortHandler     func(l *Launcher)
	interruptHandler func(l *Launcher)
	err              error
}

// NewLauncher creates a new Launcher instance
func NewLauncher(abortHandler func(l *Launcher),
	interruptHandler func(l *Launcher)) *Launcher {

	l := new(Launcher)
	l.abortHandler = abortHandler
	l.interruptHandler = interruptHandler
	l.orbiters = &sync.WaitGroup{}
	l.lock = &sync.RWMutex{}
	go l.signalHandler(make(chan (os.Signal)))
	return l
}

// Launch launches a function in a goroutine and adds it to the waitgroup
func (l *Launcher) Launch(name string, f func() error) {
	l.orbiters.Add(1)
	go func() {
		defer l.orbiters.Done()
		if err := f(); err != nil {
			l.setError(err)
			syscall.Kill(syscall.Getpid(), syscall.SIGABRT)
		}
	}()
}

func (l *Launcher) setError(err error) {
	l.lock.Lock()
	l.err = err
	l.lock.Unlock()
}

// GetError returns the last error the launcher received from a goroutine
func (l *Launcher) GetError() error {
	l.lock.Lock()
	defer l.lock.Unlock()
	return l.err
}

// Orbit calls Wait and waits for all goroutines to complete
func (l *Launcher) Orbit() {
	l.orbiters.Wait()
}

func (l *Launcher) signalHandler(sigChan chan os.Signal) {
	signal.Notify(sigChan, os.Interrupt, syscall.SIGABRT, syscall.SIGTERM, syscall.SIGQUIT)
	for sig := <-sigChan; ; sig = <-sigChan {
		switch sig {
		case syscall.SIGABRT:
			l.abortHandler(l)
		case os.Interrupt, syscall.SIGTERM, syscall.SIGQUIT:
			l.interruptHandler(l)
		}
	}
}
