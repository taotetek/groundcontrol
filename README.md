# groundcontrol
--
    import "github.com/taotetek/groundcontrol"

## Thanks

This code is inspired by techniques I picked up from Eleanor McHugh's
"A Go Developer's Notebook".  You should buy it and support her.
https://leanpub.com/GoNotebook

## Usage

#### func  SimpleAbort

```go
func SimpleAbort(l *Launcher)
```
SimpleAbort is a SIGABRT handler that just prints an error and exits

#### func  SimpleInterrupt

```go
func SimpleInterrupt(l *Launcher)
```
SimpleInterrupt is a SIGINT handler that just cleanly exits

#### type Launcher

```go
type Launcher struct {
}
```

Launcher controls the starting and managing of goroutines

#### func  NewLauncher

```go
func NewLauncher(abortHandler func(l *Launcher),
	interruptHandler func(l *Launcher)) *Launcher
```
NewLauncher creates a new Launcher instance

#### func (*Launcher) GetError

```go
func (l *Launcher) GetError() error
```
GetError returns the last error the launcher received from a goroutine

#### func (*Launcher) Launch

```go
func (l *Launcher) Launch(name string, f func() error)
```
Launch launches a function in a goroutine and adds it to the waitgroup

#### func (*Launcher) Orbit

```go
func (l *Launcher) Orbit()
```
Orbit calls Wait and waits for all goroutines to complete
